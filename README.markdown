# moreontap/unicorn

Docker image with Ruby, running Unicorn on port 3000.

## Usage

This image is a base image for unicorn-based applications, like Sinatra apps.

1. Copy this skeleton as your project, so your project looks like this

        $ tree your-project
        .
        ├── Dockerfile
        ├── README.markdown
        └── application
            ├── Gemfile
            ├── Rakefile
            ├── application.rb
            ├── config.ru
            └── unicorn.rb

2. Write your Dockerfile like this

        FROM moreontap/unicorn
        MAINTAINER Bill Nye <bill@science.guy>

3. Build

        $ docker build -t science/rules .

4. Profit

        $ docker run -d -P --name unicorn science/rules
        $ curl DOCKER_IP:`docker port unicorn | grep '^3000/tcp' | cut -d : -f 2`
